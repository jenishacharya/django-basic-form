from django import forms
from django.core import validators

from form_app.models import Topic

# this function is a custom validator
# checks if the first letter of a field valus start with 'z'
def check_for_z(value):
    if value[0].lower() != 'z':
        raise forms.ValidationError("Name has to start with 'z'")

# this class inherits from forms.Form class
class FormName(forms.Form):
    """
    MaxLengthValidator is a build in validator that checks if the length of this field exceeds the passed number
    MinLengthValidator is a build in validator that checks if the length of this field exceeds the passed number
    MinValueValidator is a build in validator that checks if the value of this field exceeds the passed number
    MinValueValidator is a build in validator that checks if the value of this field exceeds the passed number
    """
    # fields for form class
    # check_for_z is a newly made custom validator
    name = forms.CharField(validators=[check_for_z])
    number = forms.IntegerField(validators=[validators.MinValueValidator(10)])
    email = forms.EmailField()
    verify_email = forms.EmailField(label="Enter your email again")
    text = forms.CharField(widget=forms.Textarea)

    def clean(self):
        all_clean_data = super().clean()
        email = all_clean_data['email']
        vemail = all_clean_data['verify_email']

        if email != vemail:
            raise forms.ValidationError("Emails don't match!!")


class NewTopic(forms.ModelForm):
    """
    helps us to create a model for this form from a pre existing model
    this Meta class provides information connecting the model to the form
    """
    class Meta():
        model = Topic
        fields = '__all__'
