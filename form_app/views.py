from django.shortcuts import render
from . import forms
from form_app.forms import NewTopic

# Create your views here.
def index(request):
	return render(request, 'form_app/index.html')

def form_name_view(request):
	# object of created form class
	form = forms.FormName()

	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request
		form = forms.FormName(request.POST)
		# check whether it's valid        
		if form.is_valid():
			# process the data in form.cleaned_data as requested
			print("Validation Success")
			print(form.cleaned_data)
			print("Name: "+ form.cleaned_data['name'])
			print("Email: "+ form.cleaned_data['email'])
			print("Text: "+ form.cleaned_data['text'])

	return render(request, 'form_app/form_page.html', {'form': form})


def new_form(request):
	form = NewTopic()

	if request.method == 'POST':
		form = NewTopic(request.POST)
		
		if form.is_valid():
			# form.save is used to save the data in model
			form.save()
			# this line calls index function meaning once the form is submitted, we are redirected through index function
			return index(request)
		else:
			print('Error! form invalid')
	return render(request, 'form_app/form_page.html', {'form': form})

# def new_form(request):
# 	form = NewTopic()

# 	if request.method == 'POST':
# 		form = NewTopic(request.POST)

# 		if form.is_valid():
			
# 			form.save()
			
# 			return index(request)
# 		else:
# 			print('Error! form invalid')

# 	return render(request, 'form_app/form_page.html', {'form': form})


