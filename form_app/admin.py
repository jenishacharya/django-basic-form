from django.contrib import admin
from form_app.models import Topic

# Register your models here.
admin.site.register(Topic)